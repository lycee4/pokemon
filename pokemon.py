# Créé par mermet-y, le 06/04/2023 en Python 3.7
from random import random
ATK = [[0.5,1,2,2,1,1],[1,1,1,1,1,1],[1,1,0.5,1,1,1],[0.5,1,0.5,0.5,1,2],[1,1,1,1,1,2],[1,1,1,0.5,0.5,0.5]] #matrice qui stocks les faiblesses des attaquants. on trouve ceci sur internet avec la table des types
DFS = [[0.5,1,2,2,1,1],[1,1,1,1,1,1],[1,1,0.5,1,1,1],[0.5,1,0.5,0.5,1,2],[1,1,1,1,1,2],[1,1,1,0.5,0.5,0.5]]
TABLE_DES_TYPES = {"Eau" : 0, "Normal" : 1, "Electrik": 2, "Plante": 3, "Fee": 4, "Poison": 5}

def calculDegats(attaquant, defenseur):
    attaque_brute = random() * attaquant['Attaque']
    defense_brute = random() * defenseur['Defense']
    type_attaquant = attaquant["Type"]
    type_defenseur = defenseur["Type"]
    indice_type_attaquant = TABLE_DES_TYPES[type_attaquant]
    indice_type_defenseur = TABLE_DES_TYPES[type_defenseur]
    attaque_effective = attaque_brute * ATK[indice_type_attaquant][indice_type_defenseur]
    defense_effective = defense_brute * DFS[indice_type_attaquant][indice_type_defenseur]
    if defense_effective < attaque_effective:
        return attaque_effective - defense_effective
    return 0

def combat(poke1,poke2):
    premier = poke1["Vitesse"] < poke2["Vitesse"]

    if random() < 0.2:
        premier = not(premier)

    while poke1["HP"] > 9 and poke2["HP"] > 9:
        if premier:
            print("le pokemon", poke1["Nom"], "a attaqué", poke2["Nom"])
            resultat = calculDegats(poke1,poke2)
            poke2["HP"] = poke2["HP"] - resultat
        else:
            print("le pokemon", poke2["Nom"], "a attaqué", poke1["Nom"])
            resultat = calculDegats(poke2,poke1)
            poke1["HP"] = poke1["HP"] - resultat
        premier = not(premier)
        if resultat == 0:
            print("  L'attaque a raté")
        print("  Les nouveaux points de vie sont :")  #permet de clarifier l'affichage pour les points de vie
        print("    ", poke1["Nom"], ":", poke1["HP"])
        print("    ", poke2["Nom"], ":", poke2["HP"])
	
    if premier:
        print(poke1["Nom"] + " a gagné")
    else:
        print(poke2["Nom"] + " a gagné")

def main(): #plusieurs dictionnaire qui permettent de donner toutes les infos sur les pokemons
    pokemon1 = {'Nom' : 'Carapuce', "HP" : 44, "Attaque" : 48, "Defense" : 65, 'Vitesse' :43, "Type" : "Eau"}
    pokemon2 = {'Nom' : 'Evoli', "HP" : 55, "Attaque" : 55, "Defense" : 50, 'Vitesse' :55, "Type" : "Normal"}
    pokemon3 = {'Nom' : 'Pikachu', "HP" : 35, "Attaque" : 55, "Defense" : 40, 'Vitesse' :90, "Type" : "Electrik"}
    pokemon4 = {'Nom' : 'Tortipouss', "HP" : 55, "Attaque" : 68, "Defense" : 64, 'Vitesse' :31, "Type" : "Plante"}
    pokemon5 = {'Nom' : 'Rondoudou', "HP" : 115, "Attaque" : 45, "Defense" : 20, 'Vitesse' :20, "Type" : "Fee"}
    pokemon6 = {'Nom' : 'Bulbizarre', "HP" : 45, "Attaque" : 49, "Defense" : 49, 'Vitesse' :45, "Type" : "Poison"}
    
    pokemons = [pokemon1, pokemon2, pokemon3, pokemon4, pokemon5, pokemon6]


    choix1=int(input(" Quel 1er pokémon voulez-vous choisir ? (1-Carapuce, 2- Evoli, 3-Pikachu, 4-Tortipouss, 5- Rondoudou, 6- Bulbizarre): ")) #permet a l'utilisateur de choisir quels pokemons il veut faire affronter
    choix2=int(input(" Quel 2nd pokémon voulez-vous choisir ? (1-Carapuce, 2- Evoli, 3-Pikachu, 4-Tortipouss, 5- Rondoudou, 6- Bulbizarre): "))

    premier_pokemon = pokemons[choix1 - 1] 
    second_pokemon = pokemons[choix2 - 1]

    combat(premier_pokemon, second_pokemon)
main()
